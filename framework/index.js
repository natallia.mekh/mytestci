import { EmailValidation, Registration } from './services/index'

const apiProvider = () => ({
registration: () => new Registration(),
emailValidation: () => new EmailValidation(),
});

export { apiProvider};