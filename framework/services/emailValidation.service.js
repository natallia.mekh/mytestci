import { email, access_key, smtp, format } from "../config/emails";
import supertest from "supertest";
import { urls } from "../config";

const EmailValidation = function EmailValidation() {
  this.get = async function emailValidation(par1, par2) {
    const r = await supertest(urls.mailboxlayer).get("/api/check").query({
      access_key: par1,
      email: par2,
      smtp: smtp,
      format: format,
    });
    return r;
  };
};

export { EmailValidation };
