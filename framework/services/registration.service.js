import supertest from "supertest";
import { urls } from "../config";

const Registration = function Registration() {
  this.post = async function post(params) {
    const r = await supertest(urls.demo).post("/api/v1/register").send(params);
    return r;
  };
};

export { Registration };
