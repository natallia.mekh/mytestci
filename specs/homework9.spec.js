import { apiProvider } from "../framework";
import { test } from "@jest/globals";
import { access_key, email } from "../framework/config";

test("Введенный email проходит валидацию", async () => {
  const { body } = await apiProvider().emailValidation().get(access_key, email);
  expect(body.format_valid).toBe(true);
});

test.each`
  myEmail             | format_valid
  ${"test"}           | ${false}
  ${"test@"}          | ${false}
  ${"@test"}          | ${false}
  ${"test@test"}      | ${false}
  ${"test@test."}     | ${false}
  ${"test@test.com."} | ${false}
  ${".test@test.com"} | ${false}
  ${"test.@test.com"} | ${false}
  ${""}               | ${undefined}
`(
  "Email $myEmail проходит валидацию по формату = $format_valid",
  async ({ myEmail, format_valid }) => {
    const { body } = await apiProvider()
      .emailValidation()
      .get(access_key, myEmail);
    expect(body.format_valid).toBe(format_valid);
  }
);

test.each`
  access_keyMy | message
  ${""}        | ${"missing_access_key"}
  ${undefined} | ${"missing_access_key"}
  ${"12345"}   | ${"invalid_access_key"}
`(
  "Ошибка $message при использовании access key = $access_key",
  async ({ access_keyMy, message }) => {
    const { body } = await apiProvider()
      .emailValidation()
      .get(access_keyMy, email);
    expect(body.error.type).toBe(message);
  }
);
